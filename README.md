# Boolean_logic_interpretor
This is a python code that implements a boolean expression evaluator. 
The evaluator can parse and evaluate expressions in Conjunctive Normal Form (CNF) written in infix notation.

# Features
The code defines the following two functions:

1. change(z): A helper function that returns the negation of the input string. If the input is 'T', it returns 'F' and vice versa.

2. eval_bool(expression): The main evaluation function. Given a string expression representing a boolean expression, it returns its evaluation. The expression can contain the following elements:

- T and F representing the boolean values True and False, respectively.
- ∧ and ∨ representing the logical operators AND and OR, respectively.
- = representing the logical operator of equivalence.
- ~ representing the negation operator.
- let X = F or let Y = ~F representing assignment statements, where X and Y can be assigned either T or F.
- Parentheses can be used to group sub-expressions.
The eval_bool function uses the helper function change to handle negation and implements the following steps to evaluate the expression:

Replace all spaces in the expression with an empty string and convert all characters to uppercase.
- If the expression contains the string 'let', it processes the assignment statement and returns the assigned value.
- If the expression contains parentheses, it removes them and evaluates the expression within the parentheses.
- If the expression is either T or F, it returns its value.
- If the expression contains ∧ or ∨, it splits the expression into two sub-expressions and evaluates them using recursion.
- If the expression contains =, it splits the expression into two sub-expressions, evaluates them using recursion, and returns their equality.
## Installation

Install python first

```bash
https://www.python.org/downloads/
```

## Usage

```python
py boolean_logic_interpretor.py
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

