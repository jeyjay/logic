def change(z):
    if "T" in z:
        z = "F"
    else:
        z = "T"

    return z


def eval_bool(expression):
    global y
    global x
    expression = expression.replace(" ", "").upper()

    if "LET" in expression:

        #print(expression, ' in exp')
        if 'LET' not in expression:
            print(expression, ' in exp')
            if "~X" in expression:
                x1 = expression.replace("~X", change(x))
                y1 = x1.replace("Y", y)
                return y1

        else:

            left, right = expression.split("LET")
            l, r = right.split("=")
            x = r

            if '~' in r:
                s, z = r.split('~')
                #print("Values to not ", varValue, z, s)
                if z == 'T':
                    z = 'F'
                else:
                    z = 'T'
                y = z
                return str(l + ": " + z)
            elif "~" in l:
                return l
            else:
                return (l + ": " + r)

    elif "(" in expression:
        #print("Rm bracket")
        exp = expression.replace("(", "").upper()
        expr = exp.replace(")", "").upper()
        return eval_bool(expr)
    elif expression in ["T", "F"]:
        return expression == "T"
    elif "∧" in expression:
        left, right = expression.split("∧")
        #print(left, right)
        return eval_bool(left) and eval_bool(right)
    elif "∨" in expression:
        left, right = expression.split("∨")
        #print(left, right)
        return eval_bool(left) or eval_bool(right)
    elif "=" in expression:
        left, right = expression.split("=")
        return eval_bool(left) == eval_bool(right)
    else:
        raise ValueError("Invalid expression")


print(eval_bool("T ∨ F"))
print(eval_bool("(T ∧ T) = F"))
print(eval_bool("(F ∨ F) = F"))
print(eval_bool("T = F"))

print(eval_bool("let X = F"))
print(eval_bool("let Y = ~F"))
#print(eval_bool("~X ∧ Y"))
